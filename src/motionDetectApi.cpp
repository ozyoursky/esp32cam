#include <Arduino.h>
#include <eloquentarduino/vision/processing/MotionDetector.h>
#include "esp_jpg_decode.h"
#include "rom/tjpgd.h"
#include "core.h"

#define W 640
#define H 480
#define w 80
#define h 60

#define DIFF_THRESHOLD 7
#define MOTION_THRESHOLD 0.03

using namespace Eloquent::Vision;

bool decode();

const uint32_t RS_BUF_LEN = 640 * 480 / 16; //19200

struct decodeBuffer_t {
    uint8_t* buffer;
    uint32_t len;
    uint32_t index;
    uint16_t width;
} inJpeg, rsBuf;

Processing::MotionDetector<w, h> motion;

void setupMD(){
    motion.setDiffThreshold(DIFF_THRESHOLD);
    motion.setMotionThreshold(MOTION_THRESHOLD);
    // motion.setDebounceFrames(2);
}

bool doDetect(photoBuffer_t pb){
    uint8_t* redscaleBuf = (uint8_t*)heap_caps_malloc(RS_BUF_LEN, MALLOC_CAP_SPIRAM);
    inJpeg = { pb.buffer, pb.len, 0,  W };
    rsBuf = { redscaleBuf, RS_BUF_LEN, 0, w };
    decode();
    motion.detect(redscaleBuf);
    free(redscaleBuf);
    //lognum("ratio = ", motion.ratio());
    return motion.triggered();
}

static const char* jd_errors[] = {
    "Succeeded",
    "Interrupted by output function",
    "Device error or wrong termination of input stream",
    "Insufficient memory pool for the image",
    "Insufficient stream input buffer",
    "Parameter error",
    "Data format error",
    "Right format but not supported",
    "Not supported JPEG standard"
};


uint32_t jpg_read(JDEC* decoder, uint8_t* buf, uint32_t len){
    if (len > (inJpeg.len - inJpeg.index)){
        len = inJpeg.len - inJpeg.index;
    }
    if (len){
        if (buf)
            memcpy(buf, inJpeg.buffer + inJpeg.index, len);
        inJpeg.index += len;
    }
    return len;
}

uint32_t jpg_write(JDEC* decoder, void* bitmap, JRECT* rect){
    uint8_t rectW = rect->right - rect->left + 1;
    uint8_t* pixels = (uint8_t*)bitmap;
    for (int i = 0; i < rectW; i++){
        uint32_t pos = (rect->top * rsBuf.width + rect->left + i); // if top always equal to bottom
        rsBuf.buffer[pos] = pixels[3 * i];
    }
    return 1;
}

bool decode(){
    static uint8_t work[3100];
    JDEC decoder;

    JRESULT jres = jd_prepare(&decoder, jpg_read, work, 3100, nullptr);
    if (jres != JDR_OK){
        Serial.printf("JPG Header Parse Failed! %s", jd_errors[jres]);
        return ESP_FAIL;
    }

    jd_decomp(&decoder, jpg_write, (uint8_t)JPG_SCALE_8X);
    return ESP_OK;
}
