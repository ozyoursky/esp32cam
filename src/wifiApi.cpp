#include <WiFi.h>
#include "keys.h"

bool isConnectedWifi(){
  return WiFi.status() == WL_CONNECTED;
}

void setupWifi(){
  WiFi.mode(WIFI_STA);
}

void tryConnectWifi(uint16_t timeLimit){
  uint32_t start = millis();
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED && start + timeLimit > millis())  {
    Serial.print(".");
    delay(500);
  }
}
