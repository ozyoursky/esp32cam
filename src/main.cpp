#include <Arduino.h>
#include <functional>
#include "soc/rtc_cntl_reg.h"
#include <esp_task_wdt.h>
#include "core.h"
#include "telegramApi.h"
#include "cameraApi.h"
#include "wifiApi.h"
#include "otaApi.h"
#include "motionDetectionApi.h"
#include "keys.h"

#define FLASH_LED_PIN 4
#define BLINK_LED_PIN 33

typedef std::function<void()> lmbd;
void streamToSerial(photoBuffer_t jpegBuf);

enum blinkMode : uint8_t{
  LED_ON,
  LED_OFF,
  LED_1HZ,
  LED_2HZ,
  LED_5HZ,
  LED_10HZ
};

enum botAction{
  SEND_PHOTO,
  SEND_HELLO,
  SEND_CHECK_MESSAGES,
};

struct photoMessage{
  String chatId;
  photoBuffer_t photoBuf;
};

struct botNotification{
  botAction action;
  void* args;
  uint8_t argsLen;
  lmbd callback;
};

struct{
  telMessage messages[MAX_TELEGRAM_MESSAGES];
  uint8_t len;
}telMessageContainer;

QueueHandle_t botNotificationQueue;
TaskHandle_t hBlink;
TaskHandle_t hMotionDetectTask;
TaskHandle_t hMessageProcessingTask;
TaskHandle_t hBotSendingTask;
TaskHandle_t hOtaTask;
bool flashState = LOW;

static void setBlink(blinkMode mode){
  xTaskNotify(hBlink, mode, eSetValueWithOverwrite);
}

void otaTask(void* arg){
  setupOTA();
  vTaskSuspend(NULL);
  while (true){
    logln("OTA Task");
    handleOTA();
    delay(500);
  }
}

void wifiConnectingTask(void* args){
  setupWifi();
  const uint16_t CONNECCTION_TIME_LIMIT = 10000;
  while (true){
    if (!isConnectedWifi()){
      logln("wifi disconnected");
      setBlink(LED_5HZ);
      if (hBotSendingTask)
        vTaskSuspend(hBotSendingTask);
      tryConnectWifi(CONNECCTION_TIME_LIMIT);
      if (isConnectedWifi()){
        logln("wifi connected Resume tasks");
        vTaskResume(hBotSendingTask);
        setBlink(LED_1HZ);
      }
      else
        logln("no connection");
    }
    delay(2000);
  }
}

void ledTask(void* args){
  const uint16_t LED_PULSE = 25;
  pinMode(BLINK_LED_PIN, OUTPUT);
  blinkMode mode = LED_OFF;
  const bool UP_LEVEL = LOW;

  digitalWrite(BLINK_LED_PIN, !UP_LEVEL);
  while (true){
    uint32_t notifyValue;
    if (mode < LED_1HZ){
      mode = (blinkMode)notifyValue;
      if (mode == LED_OFF)
        digitalWrite(BLINK_LED_PIN, !UP_LEVEL);
      else if (mode == LED_ON)
        digitalWrite(BLINK_LED_PIN, UP_LEVEL);
      xTaskNotifyWait(0, 0, &notifyValue, portMAX_DELAY);
    }
    else{
      xTaskNotifyWait(0, 0, &notifyValue, 0);
      mode = (blinkMode)notifyValue;
      digitalWrite(BLINK_LED_PIN, UP_LEVEL);
      delay(LED_PULSE);
      digitalWrite(BLINK_LED_PIN, !UP_LEVEL);
      uint16_t delayMs;
      switch (mode){
      case LED_1HZ:
        delayMs = 1000;
        break;
      case LED_2HZ:
        delayMs = 500;
        break;
      case LED_5HZ:
        delayMs = 200;
        break;
      default:
        delayMs = 100;
        break;
      }
      delay(delayMs - LED_PULSE);
    }
  }
}

void botSendingTask(void* args){
  setupTelegramBot();
  while (true){
    botNotification notification;
    xQueueReceive(botNotificationQueue, &notification, portMAX_DELAY);
    switch (notification.action){
    case SEND_PHOTO:
    {
      photoMessage* mess = (photoMessage*)(notification.args);
      // streamToSerial(mess->photoBuf);
      sendPhotoToTelegram(&mess->photoBuf, mess->chatId);
      break;
    }
    case SEND_CHECK_MESSAGES:
      getLastMessages(telMessageContainer.messages, &telMessageContainer.len);
      break;
    case SEND_HELLO:
      logln("sending hello from task");
      break;
    default:
      logln("do nothing in task");
      break;
    }
    notification.callback();
  }
}

bool sendPhotoFromFrame(camera_fb_t* fb, String chatId = CHAT_ID){
  photoBuffer_t pb{ nullptr, fb->len };
  pb.buffer = (uint8_t*)heap_caps_malloc(fb->len, MALLOC_CAP_SPIRAM);
  memcpy(pb.buffer, fb->buf, fb->len);
  if (pb.buffer){
    photoMessage* message = new photoMessage{ CHAT_ID, pb };
    botNotification notification{
      SEND_PHOTO,
      (void*)message, 1,
      [message](){      // may slow down a little
        free(message->photoBuf.buffer);
        delete message;
      }
    };
    if (xQueueSend(botNotificationQueue, &notification, NULL) != pdTRUE){
      logln("queue is full -> deleting buffer");
      delete message;
      free(pb.buffer);
    }
  }
  else return false;
  return true;
}

void messageProcessingTask(void* args){
  String message = "running calback!";
  botNotification notification{
    SEND_CHECK_MESSAGES,
    nullptr, 0,
    [](){ xTaskNotifyGive(hMessageProcessingTask) }
  };
  while (true){
    if (xQueueSend(botNotificationQueue, &notification, pdMS_TO_TICKS(5000)) != pdTRUE){
      logln("queue is full -> resend notification");
      continue;
    }
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

    for (int i = 0; i < telMessageContainer.len; i++){
      logln(telMessageContainer.messages[i].text);
      if (telMessageContainer.messages[i].text == "/flash"){
        digitalWrite(FLASH_LED_PIN, flashState = !flashState);
      }
      else if (telMessageContainer.messages[i].text == "/photo"){
        camera_fb_t* fb = esp_camera_fb_get();
        sendPhotoFromFrame(fb, telMessageContainer.messages[i].chatId);
        esp_camera_fb_return(fb);
        logln("photo transcieved");
      }
      else if (telMessageContainer.messages[i].text == "/OTA"){
        vTaskResume(hOtaTask);
        xQueueReset(botNotificationQueue);
        xQueueSend(botNotificationQueue, &notification, NULL); //need for telegram
        vTaskSuspend(NULL);
      }
      else
        logln("do nothing");
    }
    delay(500);
  }
}

void skipFrames(int num){
  while (num--){
    camera_fb_t* fb = esp_camera_fb_get();
    photoBuffer_t pb{ fb->buf, fb->len };
    doDetect(pb);
    esp_camera_fb_return(fb);
    delay(10);
  }
}

void motionDetectTask(void* args){
  setupCamera();
  setupMD();
  skipFrames(20); // wait for camera and motion detector stabilizing
  camera_fb_t* fb;
  while (true){
   // uint32_t start = millis();
    fb = esp_camera_fb_get();
    if (fb){
      photoBuffer_t pb{ fb->buf, fb->len };
      if (doDetect(pb)){
        logln("DETECTED");
        uint8_t limit = 2;
        bool status;
        do{
          delay(100);
          status = sendPhotoFromFrame(fb);
          esp_camera_fb_return(fb);
          fb = esp_camera_fb_get();
        } while (status && --limit);
      }
     // lognum("fps ", 1000 / (millis() - start));
      esp_camera_fb_return(fb);
      fb = NULL;
    }
  }
}

void setup(){
  Serial.begin(115200);
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); // disable brownout detector
  pinMode(FLASH_LED_PIN, OUTPUT);

  xTaskCreate(ledTask, "ledTask", 1024, NULL, 1, &hBlink);
  setBlink(LED_10HZ);
  delay(1000);

  xTaskCreatePinnedToCore(wifiConnectingTask, "wifiConnectingTask", 1024 * 2, NULL, 1, NULL, 1);
  while (!isConnectedWifi())
    delay(100);

  xTaskCreatePinnedToCore(otaTask, "OTATask", 1024 * 4, NULL, 1, &hOtaTask, 1);

  botNotificationQueue = xQueueCreate(30, sizeof(struct botNotification));

  xTaskCreatePinnedToCore(motionDetectTask, "motionTask", 1024 * 5, NULL, 1, &hMotionDetectTask, 0);
  xTaskCreatePinnedToCore(messageProcessingTask, "messagesTask", 1024 * 3, NULL, 1, &hMessageProcessingTask, 1);
  xTaskCreatePinnedToCore(botSendingTask, "botSendingTask", 1024 * 5, NULL, 2, &hBotSendingTask, 1);
}

void loop(){}

void log(String message){
#ifdef ESPEYES_DEBUG
  Serial.print(message);
#endif
}

void lognum(String message, float n){
#ifdef ESPEYES_DEBUG
  Serial.print(message);
  Serial.println(n);
#endif
}

void logln(String message){
#ifdef ESPEYES_DEBUG
  Serial.println(message);
#endif
}

void streamToSerial(photoBuffer_t jpegBuf){
  logln("START OF FRAME");
  Serial.write(jpegBuf.buffer, jpegBuf.len);
  logln("END OF FRAME");
  Serial.flush();
}