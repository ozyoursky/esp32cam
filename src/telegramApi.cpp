#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include "core.h"
#include "keys.h"
#include "telegramApi.h"

WiFiClientSecure securedClient;
UniversalTelegramBot* bot;
String WELCOME_MESSAGE = "Welcome, from_name \nUse the following commands to interact with the ESP32-CAM \n/photo : takes a new photo\n/flash : toggles flash LED \n";
const String commands = F("["
  "{\"command\":\"start\", \"description\":\"Message sent when you open a chat with a bot\"},"
  "{\"command\":\"photo\",  \"description\":\"takes a new photo\"},"
  "{\"command\":\"flash\",  \"description\":\"toggles flash LED\"},"
  "{\"command\":\"status\",\"description\":\"Answer device current status\"}" // no comma on last command
  "]");

String waitForResponse(uint32_t waitTime);

void setupTelegramBot(){
  bot = new UniversalTelegramBot(BOTtoken, securedClient);
  bot->maxMessageLength = MAX_TELEGRAM_MESSAGES * 500;// for ~10 messages (HANDLE_MESSAGES 10)
  securedClient.setCACert(TELEGRAM_CERTIFICATE_ROOT);
  Serial.print("Retrieving time: ");
  configTime(0, 0, "pool.ntp.org"); // get UTC time via NTP
  time_t now = time(nullptr);
  while (now < 24 * 3600){
    Serial.print(".");
    delay(100);
    now = time(nullptr);
  }
  bot->setMyCommands(commands);
}

bool isAdminMessage(telegramMessage message){
  return message.chat_id == CHAT_ID;
}

int updateResievedMessages(){
  return bot->getUpdates(bot->last_message_received + 1);
}

void getLastMessages(telMessage container[MAX_TELEGRAM_MESSAGES], uint8_t* amount){
  *amount = updateResievedMessages();
  //lognum("updated messages: ", *amount);
  if (*amount){
    for (int i = 0; i < *amount; i++){
      lognum("message num ", i);
      if (!isAdminMessage(bot->messages[i]))
        continue;
      if (bot->messages[i].text == "/start"){
        WELCOME_MESSAGE.replace("from_name", bot->messages[i].from_name);
        bot->sendMessage(CHAT_ID, WELCOME_MESSAGE, "");
      }
      container[i] = { bot->messages[i].text, bot->messages[i].chat_id };
    }
  }
}

void sendPhotoToTelegram(photoBuffer_t* jpegBuffer, String chatId){
  const char* myDomain = "api.telegram.org";
  logln("try to connect for photo sending");
  if (!securedClient.connected())
    securedClient.connect(TELEGRAM_HOST, TELEGRAM_SSL_PORT);

  if (securedClient.connected()){
    Serial.println("Connection successful");

    String head = "--------------------------b8f610217e83e29b\r\nContent-Disposition: form-data; name=\"chat_id\"; \r\n\r\n" + CHAT_ID + "\r\n--------------------------b8f610217e83e29b\r\nContent-Disposition: form-data; name=\"photo\"; filename=\"esp32-cam.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n";
    String tail = "\r\n--------------------------b8f610217e83e29b--\r\n";

    uint16_t imageLen = jpegBuffer->len;
    uint16_t extraLen = head.length() + tail.length();
    uint16_t totalLen = imageLen + extraLen;
    securedClient.println("POST /bot" + BOTtoken + "/sendPhoto HTTP/1.1");
    securedClient.println("Host: " + String(myDomain));
    securedClient.println("Content-Length: " + String(totalLen));
    securedClient.println("Content-Type: multipart/form-data; boundary=------------------------b8f610217e83e29b");
    securedClient.println();
    securedClient.print(head);

    uint8_t* burPtr = jpegBuffer->buffer;
    for (size_t n = 0;n < jpegBuffer->len;n = n + 4096){
      if (n + 4096 < jpegBuffer->len){
        securedClient.write(burPtr, 4096);
        burPtr += 4096;
      }
      else if (jpegBuffer->len % 4096 > 0){
        securedClient.write(burPtr, jpegBuffer->len % 4096);
      }
    }
    securedClient.print(tail);
    Serial.println(waitForResponse(10000));
    //securedClient.stop(); // will closed automaticly or by sending request for new messages  
  }
  else{
    Serial.println("Connected to api.telegram.org failed.");
  }
}

String waitForResponse(uint32_t waitTime){
  long startTimer = millis();
  String getAll = "";
  String getBody = "";
  boolean state = false;

  while ((startTimer + waitTime) > millis()){
    Serial.print(".");
    delay(100);
    while (securedClient.available()){
      char c = securedClient.read();
      if (state == true) getBody += String(c);
      if (c == '\n'){
        if (getAll.length() == 0) state = true;
        getAll = "";
      }
      else if (c != '\r')
        getAll += String(c);
      startTimer = millis();
    }
    if (getBody.length() > 0) break;
  }
  return getBody;
}