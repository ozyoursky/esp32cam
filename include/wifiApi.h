#ifndef WIFI_API_H
#define WIFI_API_H

void setupWifi();
void tryConnectWifi(uint16_t timeLimit);
bool isConnectedWifi();
#endif