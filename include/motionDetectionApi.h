#ifndef MOTION_DETECTION_API_H
#define MOTION_DETECTION_API_H

void setupMD();
bool doDetect(photoBuffer_t frameBuffer);

#endif