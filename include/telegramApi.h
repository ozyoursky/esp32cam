#ifndef TELEGRAM_API_h
#define TELEGRAM_API_h

struct telMessage{
  String text;
  String chatId;
};

void setupTelegramBot();
void sendPhotoToTelegram(photoBuffer_t* jpegBuffer, String chatId);
void getLastMessages(telMessage container[MAX_TELEGRAM_MESSAGES], uint8_t* amount);

#endif