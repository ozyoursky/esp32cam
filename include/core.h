#ifndef ESPEYES_CORE_H
#define ESPEYES_CORE_H

#define ESPEYES_DEBUG
#define MAX_TELEGRAM_MESSAGES 10

struct  photoBuffer_t
{
  uint8_t* buffer;
  unsigned int len;
};

void log(String message);
void logln(String message);
void lognum(String message, float n);

#endif